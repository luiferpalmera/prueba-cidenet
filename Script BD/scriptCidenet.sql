USE [master]
GO
/****** Object:  Database [PruebaCidenet]    Script Date: 18/08/2022 9:52:23 p. m. ******/
CREATE DATABASE [PruebaCidenet]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PruebaCidenet', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\PruebaCidenet.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PruebaCidenet_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\PruebaCidenet_log.ldf' , SIZE = 22528KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PruebaCidenet] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PruebaCidenet].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PruebaCidenet] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PruebaCidenet] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PruebaCidenet] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PruebaCidenet] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PruebaCidenet] SET ARITHABORT OFF 
GO
ALTER DATABASE [PruebaCidenet] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PruebaCidenet] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [PruebaCidenet] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PruebaCidenet] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PruebaCidenet] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PruebaCidenet] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PruebaCidenet] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PruebaCidenet] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PruebaCidenet] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PruebaCidenet] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PruebaCidenet] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PruebaCidenet] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PruebaCidenet] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PruebaCidenet] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PruebaCidenet] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PruebaCidenet] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PruebaCidenet] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PruebaCidenet] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PruebaCidenet] SET RECOVERY FULL 
GO
ALTER DATABASE [PruebaCidenet] SET  MULTI_USER 
GO
ALTER DATABASE [PruebaCidenet] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PruebaCidenet] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PruebaCidenet] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PruebaCidenet] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [PruebaCidenet]
GO
/****** Object:  Table [dbo].[Areas]    Script Date: 18/08/2022 9:52:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Areas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 18/08/2022 9:52:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleados](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PrimerNombre] [nvarchar](20) NOT NULL,
	[OtrosNombres] [nvarchar](50) NOT NULL,
	[PrimerApellido] [nvarchar](20) NOT NULL,
	[SegundoApellido] [nvarchar](20) NOT NULL,
	[NumeroIdentificacion] [nvarchar](20) NOT NULL,
	[TipoIdentificacionId] [int] NOT NULL,
	[PaisId] [int] NOT NULL,
	[Correo] [nvarchar](300) NOT NULL,
	[AreaId] [int] NOT NULL,
	[FechaIngreso] [date] NOT NULL,
	[FechaRegistro] [date] NOT NULL,
	[EstadoId] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Estados]    Script Date: 18/08/2022 9:52:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paises]    Script Date: 18/08/2022 9:52:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paises](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Paises] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TiposIdentificacion]    Script Date: 18/08/2022 9:52:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TiposIdentificacion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NOT NULL,
	[Codigo] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_TiposIdentificacion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Areas] ON 

INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (1, N'Administración')
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (2, N'Financiera')
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (3, N'Compras')
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (4, N'Infraestructura')
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (5, N'Operación')
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (6, N'Talento Humano')
INSERT [dbo].[Areas] ([Id], [Nombre]) VALUES (7, N'Servicios Varios')
SET IDENTITY_INSERT [dbo].[Areas] OFF
SET IDENTITY_INSERT [dbo].[Empleados] ON 

INSERT [dbo].[Empleados] ([Id], [PrimerNombre], [OtrosNombres], [PrimerApellido], [SegundoApellido], [NumeroIdentificacion], [TipoIdentificacionId], [PaisId], [Correo], [AreaId], [FechaIngreso], [FechaRegistro], [EstadoId], [Estado]) VALUES (1, N'Luis', N'Fernando', N'Palmera', N'Escorcia', N'1081826881', 1, 1, N'luis@gmail.com', 1, CAST(N'2022-08-16' AS Date), CAST(N'2022-08-16' AS Date), 1, 1)
INSERT [dbo].[Empleados] ([Id], [PrimerNombre], [OtrosNombres], [PrimerApellido], [SegundoApellido], [NumeroIdentificacion], [TipoIdentificacionId], [PaisId], [Correo], [AreaId], [FechaIngreso], [FechaRegistro], [EstadoId], [Estado]) VALUES (3, N'Emiliano', N'', N'Carbono', N'Bolano', N'123456', 1, 1, N'emiliano.carbono@cidenet.com.co', 1, CAST(N'2022-08-17' AS Date), CAST(N'0001-01-01' AS Date), 1, 0)
INSERT [dbo].[Empleados] ([Id], [PrimerNombre], [OtrosNombres], [PrimerApellido], [SegundoApellido], [NumeroIdentificacion], [TipoIdentificacionId], [PaisId], [Correo], [AreaId], [FechaIngreso], [FechaRegistro], [EstadoId], [Estado]) VALUES (4, N'Prueba', N'', N'Apellido', N'Segundo', N'1234', 1, 1, N'prueba.apellido@cidenet.com.co', 1, CAST(N'2022-08-16' AS Date), CAST(N'2022-08-17' AS Date), 1, 1)
INSERT [dbo].[Empleados] ([Id], [PrimerNombre], [OtrosNombres], [PrimerApellido], [SegundoApellido], [NumeroIdentificacion], [TipoIdentificacionId], [PaisId], [Correo], [AreaId], [FechaIngreso], [FechaRegistro], [EstadoId], [Estado]) VALUES (5, N'Luis', N'fernando', N'Palmera', N'Escorcia', N'1081', 1, 1, N'luis.palmera@cidenet.com.co', 3, CAST(N'2022-08-16' AS Date), CAST(N'2022-08-17' AS Date), 1, 1)
SET IDENTITY_INSERT [dbo].[Empleados] OFF
SET IDENTITY_INSERT [dbo].[Estados] ON 

INSERT [dbo].[Estados] ([Id], [Nombre]) VALUES (1, N'Activo')
SET IDENTITY_INSERT [dbo].[Estados] OFF
SET IDENTITY_INSERT [dbo].[Paises] ON 

INSERT [dbo].[Paises] ([Id], [Nombre]) VALUES (1, N'Colombia')
INSERT [dbo].[Paises] ([Id], [Nombre]) VALUES (2, N'Estados Unidos')
SET IDENTITY_INSERT [dbo].[Paises] OFF
SET IDENTITY_INSERT [dbo].[TiposIdentificacion] ON 

INSERT [dbo].[TiposIdentificacion] ([Id], [Nombre], [Codigo]) VALUES (1, N'Cédula de Ciudadanía', N'CC')
INSERT [dbo].[TiposIdentificacion] ([Id], [Nombre], [Codigo]) VALUES (2, N'Cédula de Extranjería', N'CE')
INSERT [dbo].[TiposIdentificacion] ([Id], [Nombre], [Codigo]) VALUES (3, N'Pasaporte', N'PAS')
INSERT [dbo].[TiposIdentificacion] ([Id], [Nombre], [Codigo]) VALUES (4, N'Permiso Especial', N'PE')
SET IDENTITY_INSERT [dbo].[TiposIdentificacion] OFF
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Areas] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Areas]
GO
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Estados] FOREIGN KEY([EstadoId])
REFERENCES [dbo].[Estados] ([Id])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Estados]
GO
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Paises] FOREIGN KEY([PaisId])
REFERENCES [dbo].[Paises] ([Id])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Paises]
GO
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_TiposIdentificacion] FOREIGN KEY([TipoIdentificacionId])
REFERENCES [dbo].[TiposIdentificacion] ([Id])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_TiposIdentificacion]
GO
USE [master]
GO
ALTER DATABASE [PruebaCidenet] SET  READ_WRITE 
GO
