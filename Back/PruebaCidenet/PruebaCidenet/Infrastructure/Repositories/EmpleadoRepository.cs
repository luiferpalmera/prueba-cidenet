﻿using PruebaCidenet.Infrastructure.Data;
using PruebaCidenet.Infrastructure.Dto;
using PruebaCidenet.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Repositories
{
    public class EmpleadoRepository : IEmpleadoRepository
    {
        readonly private PruebaCidenetContext _applicationDbContext;
        public EmpleadoRepository(PruebaCidenetContext applicationDbContext) {
            _applicationDbContext = applicationDbContext;
        }
        public async Task<DataReferenciaDto> GetDataReferencia()
        {
            var dataReferencia = new DataReferenciaDto();
            dataReferencia.Areas = _applicationDbContext.Areas.ToList();
            dataReferencia.TiposIdentificacion = _applicationDbContext.TiposIdentificacions.ToList();
            dataReferencia.Paises = _applicationDbContext.Paises.ToList();
            return dataReferencia;
        }

        public async Task<List<EmpleadoDto>> GetEmpleados()
        {
            var data = _applicationDbContext.Empleados.Select(x=> new EmpleadoDto() { 
                AreaId = x.AreaId,
                AreaNombre = x.Area.Nombre,
                CodigoIdentificacion = x.TipoIdentificacion.Codigo,
                Correo = x.Correo,
                FechaIngreso = x.FechaIngreso,
                Id = x.Id,
                NumeroIdentificacion = x.NumeroIdentificacion,
                OtrosNombres = x.OtrosNombres,
                PaisId = x.PaisId,
                PaisNombre = x.Pais.Nombre,
                PrimerApellido = x.PrimerApellido,
                PrimerNombre = x.PrimerNombre,
                SegundoApellido = x.SegundoApellido,
                TipoIdentificacionId = x.TipoIdentificacionId,
                TipoIdentificacionNombre = x.TipoIdentificacion.Nombre,
                Estado = x.Estado
            }).ToList();
            return data;
        }

        public List<Empleado> GetEmpleadoByCorreo(string prefijo, string sufijo)
        {
            var data = _applicationDbContext.Empleados.Where(x=> x.Correo.Contains(prefijo) && x.Correo.Contains(sufijo)).ToList();
            return data;
        }
        public async Task<List<Empleado>> GetEmpleadosByDocumento(string numeroIdentificacion, int tipoIdentificacionId)
        {
            var data = _applicationDbContext.Empleados.Where(x => x.NumeroIdentificacion == numeroIdentificacion && x.TipoIdentificacionId == tipoIdentificacionId).ToList();
            return data;
        }

        public async Task<List<Empleado>> GetEmpleadosById(int id)
        {
            var data = _applicationDbContext.Empleados.Where(x => x.Id == id).ToList();
            return data;
        }

        public async Task<GenericResponseDto> GuardarEmpleado(EmpleadoDto empleado)
        {

            var empleadoGuardar = await _applicationDbContext.Empleados.FindAsync(empleado.Id);
            bool swNuevoEmpleado = false;
            if (empleadoGuardar == null) {
                empleadoGuardar = new Empleado();
                empleadoGuardar.EstadoId = 1;
                empleadoGuardar.Estado = true;
                empleadoGuardar.FechaRegistro = DateTime.Now;
                swNuevoEmpleado = true;
            }

            empleadoGuardar.AreaId = empleado.AreaId;
            empleadoGuardar.PaisId = empleado.PaisId;
            empleadoGuardar.TipoIdentificacionId = empleado.TipoIdentificacionId;
            empleadoGuardar.FechaIngreso = empleado.FechaIngreso;
            empleadoGuardar.NumeroIdentificacion = empleado.NumeroIdentificacion;
            empleadoGuardar.OtrosNombres = empleado.OtrosNombres;
            empleadoGuardar.PrimerNombre = empleado.PrimerNombre;
            empleadoGuardar.PrimerApellido = empleado.PrimerApellido;
            empleadoGuardar.SegundoApellido = empleado.SegundoApellido;
            empleadoGuardar.Correo = empleado.Correo != empleadoGuardar.Correo ? empleado.Correo : empleadoGuardar.Correo;
            if (swNuevoEmpleado) {
                var data = _applicationDbContext.Empleados.Add(empleadoGuardar);
            }            

            await _applicationDbContext.SaveChangesAsync();

            return new GenericResponseDto{ Success = true};
        }
        public async Task<GenericResponseDto> EliminarEmpleado(int id) {
            var empleadoEliminar = await _applicationDbContext.Empleados.FindAsync(id);
            empleadoEliminar.Estado = false;
            await _applicationDbContext.SaveChangesAsync();
            return new GenericResponseDto { Success = true };
        }
    }
}
