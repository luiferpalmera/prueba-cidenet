﻿using PruebaCidenet.Infrastructure.Data;
using PruebaCidenet.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Repositories.Interfaces
{
    public interface IEmpleadoRepository
    {
        public Task<List<EmpleadoDto>> GetEmpleados();
        public Task<DataReferenciaDto> GetDataReferencia();
        public List<Empleado> GetEmpleadoByCorreo(string prefijo, string sufijo);
        public Task<List<Empleado>> GetEmpleadosById(int id);
        public Task<GenericResponseDto> GuardarEmpleado(EmpleadoDto empleado);
        public Task<List<Empleado>> GetEmpleadosByDocumento(string numeroIdentificacion, int tipoIdentificacionId);
        public Task<GenericResponseDto> EliminarEmpleado(int id);
    }
}
