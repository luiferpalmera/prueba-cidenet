﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PruebaCidenet.Infrastructure.Data
{
    public partial class Area
    {
        public Area()
        {
            Empleados = new HashSet<Empleado>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
    }
}
