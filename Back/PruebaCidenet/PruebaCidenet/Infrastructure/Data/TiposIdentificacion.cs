﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PruebaCidenet.Infrastructure.Data
{
    public partial class TiposIdentificacion
    {
        public TiposIdentificacion()
        {
            Empleados = new HashSet<Empleado>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }
    }
}
