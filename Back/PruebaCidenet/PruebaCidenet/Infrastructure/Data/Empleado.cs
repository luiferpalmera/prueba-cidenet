﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PruebaCidenet.Infrastructure.Data
{
    public partial class Empleado
    {
        public int Id { get; set; }
        public string PrimerNombre { get; set; }
        public string OtrosNombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NumeroIdentificacion { get; set; }
        public int TipoIdentificacionId { get; set; }
        public int PaisId { get; set; }
        public string Correo { get; set; }
        public int AreaId { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int EstadoId { get; set; }
        public bool Estado { get; set; }

        public virtual Area Area { get; set; }
        public virtual Estado EstadoNavigation { get; set; }
        public virtual Paise Pais { get; set; }
        public virtual TiposIdentificacion TipoIdentificacion { get; set; }
    }
}
