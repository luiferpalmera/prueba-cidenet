﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace PruebaCidenet.Infrastructure.Data
{
    public partial class PruebaCidenetContext : DbContext
    {
        public PruebaCidenetContext()
        {
        }

        public PruebaCidenetContext(DbContextOptions<PruebaCidenetContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<Empleado> Empleados { get; set; }
        public virtual DbSet<Estado> Estados { get; set; }
        public virtual DbSet<Paise> Paises { get; set; }
        public virtual DbSet<TiposIdentificacion> TiposIdentificacions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=ApplicationDbContext");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Area>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Empleado>(entity =>
            {
                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.FechaIngreso).HasColumnType("date");

                entity.Property(e => e.FechaRegistro).HasColumnType("date");

                entity.Property(e => e.NumeroIdentificacion)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.OtrosNombres)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PrimerApellido)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.PrimerNombre)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.SegundoApellido)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.AreaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empleados_Areas");

                entity.HasOne(d => d.EstadoNavigation)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.EstadoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empleados_Estados");

                entity.HasOne(d => d.Pais)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.PaisId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empleados_Paises");

                entity.HasOne(d => d.TipoIdentificacion)
                    .WithMany(p => p.Empleados)
                    .HasForeignKey(d => d.TipoIdentificacionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empleados_TiposIdentificacion");
            });

            modelBuilder.Entity<Estado>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Paise>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<TiposIdentificacion>(entity =>
            {
                entity.ToTable("TiposIdentificacion");

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
