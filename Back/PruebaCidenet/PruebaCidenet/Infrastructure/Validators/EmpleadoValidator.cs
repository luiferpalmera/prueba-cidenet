﻿using FluentValidation;
using PruebaCidenet.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Validators
{
    public class EmpleadoValidator : AbstractValidator<EmpleadoDto>
    {
        public EmpleadoValidator()
        {
            RuleFor(x => x.PrimerNombre).NotNull();
            RuleFor(x => x.PrimerNombre).Matches(@"^[A-Za-z ]+$");
            RuleFor(x => x.PrimerNombre).Length(1, 20);

            RuleFor(x => x.OtrosNombres).Length(0, 50);
            When(x => !string.IsNullOrEmpty(x.OtrosNombres), ()=> { RuleFor(x => x.OtrosNombres).Matches(@"^[A-Za-z ]+$"); });

            RuleFor(x => x.PrimerApellido).NotNull();
            RuleFor(x => x.PrimerApellido).Matches(@"^[A-Za-z ]+$");
            RuleFor(x => x.PrimerApellido).Length(1, 20);

            RuleFor(x => x.SegundoApellido).NotNull();
            RuleFor(x => x.SegundoApellido).Matches(@"^[A-Za-z ]+$");
            RuleFor(x => x.SegundoApellido).Length(1, 20);

            RuleFor(x => x.NumeroIdentificacion).NotNull();
            RuleFor(x => x.NumeroIdentificacion).Matches(@"^[a-zA-Z0-9-]+$");
            RuleFor(x => x.NumeroIdentificacion).Length(1, 20);

            RuleFor(x => x.AreaId).NotNull();
            RuleFor(x => x.TipoIdentificacionId).NotNull();
            RuleFor(x => x.PaisId).NotNull();

            DateTime fechaActual = DateTime.Now;

            RuleFor(x => x.FechaIngreso).NotNull();
            RuleFor(x => x.FechaIngreso).LessThanOrEqualTo(fechaActual).GreaterThanOrEqualTo(fechaActual.AddMonths(-1));

        }
    }
}
