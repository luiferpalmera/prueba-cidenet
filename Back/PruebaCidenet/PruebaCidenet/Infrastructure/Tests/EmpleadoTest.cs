﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PruebaCidenet.Infrastructure.Dto;
using PruebaCidenet.Infrastructure.Managers;
using PruebaCidenet.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Tests
{
    [TestClass]
    public class EmpleadoTest
    {
        private readonly EmpleadoManager _empleadoManager;
        readonly private IEmpleadoRepository _empleadoRepository;
        private readonly ILogger<EmpleadoManager> _logger;

        public EmpleadoTest()
        {
            _empleadoManager = new EmpleadoManager(_empleadoRepository, _logger);
        }
        [TestMethod]
        public void ConstruirCorreo()
        {
            var empleado = new EmpleadoDto();
            empleado.AreaId = 1;
            empleado.PrimerNombre = "Luis";
            empleado.PrimerApellido = "Palmera";
            empleado.PaisId = 1;
            var correo = _empleadoManager.ConstruirCorreo(empleado);
            Assert.AreEqual(correo, "luis.palmera@cidenet.com.co");
        }

        [TestMethod]
        public void ObtenerSufijoCorreo()
        {
            var sufijo = _empleadoManager.ObtenerSufijoCorreo(1);
            Assert.AreEqual(sufijo, "@cidenet.com.co");
        }
        [TestMethod]
        public void ValidateCorreo()
        {
            var mock = new Mock<IEmpleadoRepository>();
            var resultMock = mock.Setup(p => p.GetEmpleadoByCorreo("luis.palmera", "@cidenet.com.co").Count).Returns(0);
            var resultCorreo = _empleadoManager.ValidateCorreo("luis.palmera@cidenet.com.co");
            Assert.AreEqual(resultCorreo, "luis.palmera@cidenet.com.co");
        }
        [TestMethod]
        public void ValidateCorreoConExistencias()
        {
            var mock = new Mock<IEmpleadoRepository>();
            var resultMock = mock.Setup(p => p.GetEmpleadoByCorreo("luis.palmera","@cidenet.com.co").Count).Returns(2);
            var resultCorreo = _empleadoManager.ValidateCorreo("luis.palmera.2@cidenet.com.co");
            Assert.AreEqual(resultCorreo, "luis.palmera.3@cidenet.com.co");
        }
    }
}
