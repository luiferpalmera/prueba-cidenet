﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Dto
{
    public class EmpleadoDto
    {
        public int? Id { get; set; }
        public string PrimerNombre { get; set; }
        public string OtrosNombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NumeroIdentificacion { get; set; }
        public int TipoIdentificacionId { get; set; }
        public int PaisId { get; set; }
        public string Correo { get; set; }
        public int AreaId { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string PaisNombre { get; set; }
        public string AreaNombre { get; set; }
        public string CodigoIdentificacion { get; set; }
        public string TipoIdentificacionNombre { get; set; }
        public bool Estado { get; set; }
    }
}
