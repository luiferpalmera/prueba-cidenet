﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Dto
{
    public class GenericResponseDto
    {
        public bool Success { get; set; }
        public List<string> Errores { get; set; }

    }
}
