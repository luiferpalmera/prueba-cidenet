﻿using PruebaCidenet.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Dto
{
    public class DataReferenciaDto
    {
        public List<Area> Areas { get; set; }
        public List<Paise> Paises { get; set; }
        public List<TiposIdentificacion> TiposIdentificacion { get; set; }
    }
}
