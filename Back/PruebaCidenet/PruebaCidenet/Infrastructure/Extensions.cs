﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PruebaCidenet.Infrastructure.Data;
using PruebaCidenet.Infrastructure.Dto;
using PruebaCidenet.Infrastructure.Managers;
using PruebaCidenet.Infrastructure.Managers.Interfaces;
using PruebaCidenet.Infrastructure.Repositories;
using PruebaCidenet.Infrastructure.Repositories.Interfaces;
using PruebaCidenet.Infrastructure.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure
{
    public static class Extensions
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IEmpleadoManager, EmpleadoManager>();
            services.AddScoped<IEmpleadoRepository, EmpleadoRepository>();
            services.AddDbContext<PruebaCidenetContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("ApplicationDbContext")));
            services.AddTransient<IValidator<EmpleadoDto>, EmpleadoValidator>();
        }
    }
}
