﻿using Microsoft.Extensions.Logging;
using PruebaCidenet.Infrastructure.Data;
using PruebaCidenet.Infrastructure.Dto;
using PruebaCidenet.Infrastructure.Managers.Interfaces;
using PruebaCidenet.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Managers
{
    public class EmpleadoManager : IEmpleadoManager
    {
        readonly private IEmpleadoRepository _empleadoRepository;
        private readonly ILogger<EmpleadoManager> _logger;
        public EmpleadoManager(IEmpleadoRepository empleadoRepository, ILogger<EmpleadoManager> logger) {
            _empleadoRepository = empleadoRepository;
            _logger = logger;
        }
        public Task<List<EmpleadoDto>> GetEmpleados()
        {
            return _empleadoRepository.GetEmpleados();
        }
        public Task<List<Empleado>> GetEmpleadosById(int id)
        {
            return _empleadoRepository.GetEmpleadosById(id);
        }

        public async Task<GenericResponseDto> GuardarEmpleado(EmpleadoDto empleado)
        {
            if (empleado.Id == null && _empleadoRepository.GetEmpleadosByDocumento(empleado.NumeroIdentificacion, empleado.TipoIdentificacionId).Result.Count > 0) {
                _logger.LogError("Se encontró registro de empleado");
                List<string> errores = new List<string>() { "Se encontró empleado con el mismo documento de identificación"};
                return new GenericResponseDto() { Success = false, Errores = errores };
            }
            empleado.Correo = ValidateCorreo(ConstruirCorreo(empleado));
            return await _empleadoRepository.GuardarEmpleado(empleado);
        }

        public string ConstruirCorreo(EmpleadoDto empleado)
        {
            _logger.LogError("Entra a construir correo");
            string correo = empleado.PrimerNombre + "." + empleado.PrimerApellido + ObtenerSufijoCorreo(empleado.PaisId);
            return correo.ToLower();
        }
        public string ObtenerSufijoCorreo(int paisId)
        {
            _logger.LogError("Entra a obtener sufijo de correo");
            return paisId == 1 ? "@cidenet.com.co" : "@cidenet.com.us";
        }
        public string ValidateCorreo(string correo)
        {
            _logger.LogError("Entra a validar y setear correo");
            var splitCorreoPorPunto = correo.Split(".");
            var splitCorreoPorArroba = correo.Split("@");
            var empleadosCantidad = _empleadoRepository.GetEmpleadoByCorreo(splitCorreoPorArroba[0], splitCorreoPorArroba[1]).Count;

            if (empleadosCantidad > 0)
            {
                correo = splitCorreoPorArroba[0] + "." + (empleadosCantidad) + "@" + splitCorreoPorArroba[1];
            }
            return correo;
        }
        public async Task<GenericResponseDto> EliminarEmpleado(int id) {
            return await _empleadoRepository.EliminarEmpleado(id);
        }
        public async Task<DataReferenciaDto> GetDataReferencia()
        {
            return await _empleadoRepository.GetDataReferencia();
        }
    }
}
