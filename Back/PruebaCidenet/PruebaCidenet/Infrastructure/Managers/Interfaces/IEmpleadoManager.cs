﻿using PruebaCidenet.Infrastructure.Data;
using PruebaCidenet.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaCidenet.Infrastructure.Managers.Interfaces
{
    public interface IEmpleadoManager
    {
        public Task<GenericResponseDto> GuardarEmpleado(EmpleadoDto empleado);
        public Task<GenericResponseDto> EliminarEmpleado(int id);
        public Task<List<EmpleadoDto>> GetEmpleados();
        public Task<List<Empleado>> GetEmpleadosById(int id);
        public Task<DataReferenciaDto> GetDataReferencia();
    }
}
