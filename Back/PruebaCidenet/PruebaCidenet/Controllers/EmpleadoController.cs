﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaCidenet.Infrastructure.Data;
using PruebaCidenet.Infrastructure.Dto;
using PruebaCidenet.Infrastructure.Managers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PruebaCidenet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoController : ControllerBase
    {
        private readonly IEmpleadoManager _empleadoManager;
        private readonly ILogger<EmpleadoController> _logger;
        public EmpleadoController(IEmpleadoManager empleadoManager, ILogger<EmpleadoController> logger) {
            _empleadoManager = empleadoManager;
            _logger = logger;
        }
        // GET: api/<EmpleadoController>
        [HttpGet]
        public async Task<List<EmpleadoDto>> GetAsync()
        {
            _logger.LogInformation("Entra a método para obtener listado de empleados");
            return await _empleadoManager.GetEmpleados();
        }

        // GET api/<EmpleadoController>/5
        [HttpGet("{id}")]
        public async Task<List<Empleado>> Get(int id)
        {
            _logger.LogInformation("Entra a método para obtener información de un empleado");
            return await _empleadoManager.GetEmpleadosById(id);
        }

        // POST api/<EmpleadoController>
        [HttpPost]
        public async Task<GenericResponseDto> Post([FromBody] EmpleadoDto empleado)
        {
            _logger.LogInformation("Entra a método para guardar información de empleado");
            return await _empleadoManager.GuardarEmpleado(empleado);
        }

        // PUT api/<EmpleadoController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmpleadoController>/5
        [HttpDelete("{id}")]
        public async Task<GenericResponseDto> Delete(int id)
        {
            _logger.LogInformation("Entra a método para eliminar empleado");
            return await _empleadoManager.EliminarEmpleado(id);
        }
        [Route("GetDataReferencia")]
        [HttpGet]
        public async Task<DataReferenciaDto> GetDataReferencia()
        {
            _logger.LogInformation("Entra a método para obtener información de referencia");
            return await _empleadoManager.GetDataReferencia();
        }
    }
}
