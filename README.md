Aplicación Front:

Angular CLI: 14.1.2
Node: 14.15.5

Al clonar la aplicación, ejecutar la instrucción npm install. Así mismo, para correr la aplicación con el comando ng s.

Aplicación Back:
.NetCore 5
Para las reglas de validaciones, se usó FluentValidation
Para las pruebas unitarias se hizo uso de Moq
Para el manejo de logs se realizó a través de Microsoft.Extensions.Logging

En la carpeta "Script BD" se encuentra el script con la estructura y datos de pruebas, para crear la base de datos y poder hacer uso de la aplicación.

Para la configuración de nivel de datos, en el archivo ppsettings.json se debe modificar lo siguiente
"ConnectionStrings": {
    "ApplicationDbContext": "Data Source=HostBasedeDatos,1433;Initial Catalog=PruebaCidenet;User ID=userBaseDatos;Password=passwordBaseDatos;"
  }
