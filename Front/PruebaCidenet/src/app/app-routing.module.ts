import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { GuardarEmpleadoComponent } from './components/guardar-empleado/guardar-empleado.component';

const routes: Routes = [
  { path: 'empleados', component: EmpleadosComponent },
  { path: 'guardar-empleado', component: GuardarEmpleadoComponent },
  { path: 'guardar-empleado/:id', component: GuardarEmpleadoComponent },
  { path: '**', pathMatch: 'full', redirectTo:'/empleados' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
