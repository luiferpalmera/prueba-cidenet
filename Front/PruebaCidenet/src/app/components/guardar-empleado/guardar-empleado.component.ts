import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Area } from 'src/app/clases/Area';
import { Pais } from 'src/app/clases/Pais';
import { TipoIdentificacion } from 'src/app/clases/TipoIdentificacion';
import { EmpleadoService } from 'src/app/services/empleado.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-guardar-empleado',
  templateUrl: './guardar-empleado.component.html',
  styleUrls: ['./guardar-empleado.component.css']
})
export class GuardarEmpleadoComponent implements OnInit {
  public formEmpleado: FormGroup ;
  errores:any[]=[];
  paises : Pais[] = [];
  areas:Area[] = [];
  tiposIdentificacion:TipoIdentificacion[] = [];
  public fechaActual:Date;
  public fechaMinima:Date;
  public id:number = 0;
  constructor(private _empleadoService:EmpleadoService,private spinner: NgxSpinnerService, private _formBuilder: FormBuilder, private rout: ActivatedRoute) {
    this.formEmpleado = this._formBuilder.group({
      id: [],
      correo: [],
      tipoIdentificacionNombre: [],
      paisNombre: [],
      tipoIdentificacionId: [1, Validators.required],
      numeroIdentificacion : ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9-]+$')]],
      primerNombre: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9 ]+$')]],
      otrosNombres: ['',[Validators.maxLength(50),Validators.pattern('^[A-Za-z0-9]+$')]],
      primerApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9]+$')]],
      segundoApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9]+$')]],
      paisId: [1, Validators.required],
      fechaIngreso: [moment(new Date()).format("YYYY-MM-DD"), Validators.required],
      areaId: [, Validators.required]
    });
    this.fechaActual = new Date();
    this.fechaMinima = new Date();

  }

  ngOnInit(): void {
    this.spinner.show();
    this._empleadoService.getDataReferencia().subscribe(data => {
      this.paises = data['paises'];
      this.areas = data['areas'];
      this.tiposIdentificacion = data['tiposIdentificacion'];
      this.spinner.hide();
    });
    this.id = this.rout.snapshot.params['id'];

    if(this.id){
      this.spinner.show();
      this._empleadoService.getEmpleadoById(this.id).subscribe(data => {
        let dataInfo = data[0];
        for ( const atributos  in this.formEmpleado.value) {
          this.formEmpleado.get(atributos)?.setValue(dataInfo[atributos]);
        }
        this.spinner.hide();
      });
    }
    this.calcularFechaMinima();
  }
  construirDataReferencia(){

  }

  guardarEmpleado(){
    if( !this.validateForm() ){ return; }
    this.spinner.show();
    this._empleadoService.guardarEmpleado(this.formEmpleado.value).subscribe(data => {
      console.log(data);
      if(data.success){
        Swal.fire({
          title: 'Realizado!',
          text: 'Acción realizada satisfactoriamente.',
          icon: 'success',
          timer: 2000,
          confirmButtonText: 'Ok'
        })

        setTimeout(() => {
          window.location.href = "/empleados"
        }, 2000);
      }else{
        this.errores = data.errores;
      }

    },err =>{
      this.construirErrores(err.error.errors);
      Swal.fire({
        title: 'Error!',
        text: 'No se pudo efectuar la operación. Intente más tarde.',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
    })
    this.spinner.hide();
  }
  construirErrores(errores:any){
    for (const erroresProperty in errores) {
      console.log(errores[erroresProperty]);
      this.errores = this.errores.concat(errores[erroresProperty]);
    }
    console.log("prueba",this.errores)
  }

  validateForm(){
    if (this.formEmpleado.invalid) {
      Object.values(this.formEmpleado.controls).forEach(control => {
        if (control instanceof FormGroup) {
          Object.values(control.controls).forEach(control => control.markAsTouched());
        } else {
          control.markAsTouched();
        }
      });
      return false;
    }
    return true;
  }
  calcularFechaMinima(){
    this.fechaMinima.setMonth(this.fechaMinima.getMonth() - 1);
  }

}
