export interface Empleado{
    id?:number;
    primerApellido:string;
    segundoApellido:string;
    primerNombre:string;
    otrosNombres:string;
    paisId:number;
    tipoIdentificacionId:number;
    numeroIdentificacion:string;
    correo:string;
    paisNombre:string,
    tipoIdentificacionNombre: string,
    areaNombre: string,
    codigoIdentificacion: string,
    estado: boolean
}
