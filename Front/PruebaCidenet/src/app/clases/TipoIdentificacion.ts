export interface TipoIdentificacion{
    id:number;
    nombre:string;
    codigo:string;
}