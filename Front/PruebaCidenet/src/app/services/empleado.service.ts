import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Empleado } from '../clases/Empleado';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  headers = { headers: new HttpHeaders({})};


  constructor(private httpClient:HttpClient) { }

    getEmpleados(): Observable<any>{
        return this.httpClient.get(environment.api+'Empleado',this.headers)
    }
    guardarEmpleado(empleado : Empleado): Observable<any>{
      return this.httpClient.post(environment.api+'Empleado',empleado, this.headers);
    }
    eliminarEmpleado(id:number | undefined): Observable<any>{
      return this.httpClient.delete(environment.api+'Empleado/'+id, this.headers);
    }
    getDataReferencia(): Observable<any>{
        return this.httpClient.get(environment.api+'Empleado/GetDataReferencia')
    }
    getEmpleadoById(id:number): Observable<any>{
        return this.httpClient.get(environment.api+'Empleado/'+id)
    }

}
